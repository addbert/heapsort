import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class SimpleView extends JFrame
{

    private JPanel contentPane, top1, top2, topp, bottomp;
    private JScrollPane scrollpane;
    private JLabel inlabel, searchlabel;
    
    public JTextField searchField, inputField;
    public JButton search_button, randombutton, button_a, inputbutton, closebutton;
    public JTextArea outputArea;
    /**
     * Create the frame.
     */
    public SimpleView()
    {
    	this.setTitle("HeapSort-Algorithmus by Adrian Neubert");
	
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setBounds(100, 100, 750, 750);
	this.contentPane = new JPanel();
	this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	this.setContentPane(contentPane);
	this.contentPane.setLayout(new BorderLayout(0, 0));
	
	this.topp = new JPanel();
	this.contentPane.add(topp, BorderLayout.NORTH);
	this.topp.setLayout(new GridLayout(2, 0, 0, 0));
	
	this.top1 = new JPanel();
	this.topp.add(top1);
	this.top1.setLayout(new GridLayout(1, 0, 0, 0));
	
	this.button_a = new JButton("Feld aus Aufgabe sortieren.");
	this.top1.add(button_a);
	
	this.randombutton = new JButton("Zuf\u00E4lliges Feld sortieren.");
	this.top1.add(randombutton);
	
	this.top2 = new JPanel();
	this.topp.add(top2);
	
	this.inlabel = new JLabel("Feld eingeben");
	this.top2.add(inlabel);
	
	this.inputField = new JTextField();
	this.inputField.setText("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
	this.top2.add(inputField);
	this.inputField.setColumns(40);
	
	this.inputbutton = new JButton("Sortieren");
	this.top2.add(inputbutton);
	
	this.scrollpane = new JScrollPane();
	this.contentPane.add(scrollpane, BorderLayout.CENTER);
	
	this.outputArea = new JTextArea();
	this.outputArea.setEditable(false);
	this.scrollpane.setViewportView(outputArea);
	
	this.bottomp = new JPanel();
	this.contentPane.add(bottomp, BorderLayout.SOUTH);
	
	this.searchlabel = new JLabel("Element suchen");
	this.bottomp.add(searchlabel);
	
	this.searchField = new JTextField();
	this.searchField.setText("0");
	this.bottomp.add(searchField);
	this.searchField.setColumns(10);
	
	this.search_button = new JButton("Suchen");
	this.bottomp.add(search_button);
	
	this.closebutton = new JButton("Beenden!");
	this.bottomp.add(closebutton);
	
	this.setVisible(true);
    }

}
