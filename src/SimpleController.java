import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class SimpleController implements ActionListener
{
    public SimpleView view;
    public SortnSearch model;
    
    public void initialise(SimpleView sv, SortnSearch sns)
    {
	this.view = sv;
	this.model = sns;

	view.button_a.addActionListener(this);
	view.randombutton.addActionListener(this);
	view.search_button.addActionListener(this);
	view.inputbutton.addActionListener(this);
	view.closebutton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
	if (e.getSource() == view.button_a)
	{
	    int[] example =
	    { 6, 2, 3, 8, 7, 5, 14, 10, 12, 1, 11, 13 };
	    model.field = example;
	    this.work();
	}
	if (e.getSource() == view.randombutton)
	{
	    Random r = new Random();
	    int rz = r.nextInt(30);
	    int[] random = new int[rz];
	    for (int i = 0; i < rz - 1; i++)
	    {
		Integer z = r.nextInt(30);
		random[i] = z;
	    }

	    model.field = random;
	    this.work();
	}
	if (e.getSource() == view.search_button)
	{
	    try
	    {
		int i = Integer.parseInt(view.searchField.getText());
		int found = model.binarySearch(i);
		if (found >= 0)
		{
		    view.outputArea.append("Das Element " + i + " wurde an Pos: " + found + " gefunden. \n");
		} else
		{
		    view.outputArea.append("Das gesuchte Element " + i + " wurde nicht gefunden! \n");
		}
	    } catch (Exception ex)
	    {
		view.outputArea.append("Eingabe konnte nicht verarbeitet werden! \n");
		view.outputArea.append("Versuchen sie es erneut. \n");
	    }

	}
	if (e.getSource() == view.inputbutton)
	{
	    try
	    {
		String line = view.inputField.getText();
		String[] aline = line.split(",");
		int[] tosort = new int[aline.length];
		for (int i = 0; i < aline.length; i++)
		{
		    tosort[i] = Integer.parseInt(aline[i]);
		}
		model.field = tosort;
		this.work();

	    } catch (NumberFormatException ex)
	    {
		view.outputArea.append("Bitte nur mit Komma getrennte Integer-Werte eingeben! \n");
	    }
	}
	if(e.getSource() == view.closebutton)
	{
	    System.exit(0);
	}
    }

    private void work()
    {
	view.outputArea.setText(null);
	view.outputArea.append("Eingangsfeld: \n");
	view.outputArea.append(Arrays.toString(model.field) + "\n");
	model.sort();
	view.outputArea.append("HEAP: \n");
	view.outputArea.append(Arrays.toString(model.heap) + "\n");
	view.outputArea.append(model.steps.toString());
	view.outputArea.append("Sortierte Feld: \n");
	view.outputArea.append(Arrays.toString(model.field) + "\n");
    }
}
