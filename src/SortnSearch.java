import java.util.Arrays;

public class SortnSearch
{
    public StringBuilder steps;
    public int[] field, heap;
    
    public int binarySearch(int key)
    {
	final int notfound = -1;
	int lower = 0;
	int upper = field.length-1;
	int middle;
	
	
	while(lower <= upper)
	{
	    middle = lower + (upper - lower) / 2;
	    if(field[middle] == key)
	    {
 		return middle + 1;
	    }
	    else
	    {
		if(field[middle] > key)
		{
		    upper = middle -1;
		}
		else
		{
		    lower = middle +1;
		}
	    }
	}
	
	return notfound;
    }
    public void sort()
    {
	steps  = new StringBuilder("Zwischenschritte: \n");
	//1. Heap erzeugen
	buildHeap(); 
	
	// Eigentliches Sortieren findet hier statt.
	// Bei jedem Durchlauf wird das Array um 1 gek�rzt.
	for (int i = field.length - 1; i > 0; i--)
	{
	    //2. Vertauschen des 1. und letzten Elements im Heap
	    swap(i, 0); 
	    
	    // 3. Heap-Bedingung wiederherstellen
	    shiftdown(0, i);
	    
	    steps.append(Arrays.toString(this.field) + "\n"); // Das Array bei jedem Tauschen abspeichern um es sp�ter anzuzeigen.
	}
    }
    
    // Beim letzten Element mit Kinderknoten anfangen und Heap-Bedingung erf�llen
    private void buildHeap()
    {
	for (int i = (field.length / 2) - 1; i >= 0; i--)
	{
	    shiftdown(i, field.length);
	}
	 heap = this.field.clone(); 
    }
    
    //Elemente im Heap versenken
    private void shiftdown(int i, int l)
    {
	while(i <= (l / 2) - 1)
	    {
		int child = ((i + 1) * 2) - 1; //Index des Linken Kindes
		
		//Pr�fen ob ein Kind auf der Rechtenseite existiert
		if(child + 1 <= l -1)
		{
		    //Wenn ein rechtes Kind existiert
		    if(field[child] < field[child + 1])
		    {
			child ++; // Wenn rechtes Kind gr��er ist
		    }
		}
		//Schaue ob eine Element im Heap "sinken" muss
		if(field[i] < field[child])
		{
		    swap(i,child);// Vertauschen der beiden Elemente
		    i = child; // An neuer Positien wiederholen
		}
		else
		    break;
	    }
    }
    
    //Vertauscht die Positionen von i und j im Array
    private void swap ( int i, int j)
    {
	int tmp = field[i];
	field[i] = field[j];
	field[j] = tmp;
    }
}