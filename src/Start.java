public class Start
{
    public static void main(String[] args) throws Exception
    {
	SimpleView view = new SimpleView();	
	SortnSearch model = new SortnSearch();
	SimpleController controller = new SimpleController();
	controller.initialise(view, model);
    }
}
